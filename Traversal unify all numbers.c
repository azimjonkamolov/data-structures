// Traversal unify all numbers.c
#include<stdio.h>
#include<math.h>
int main()
{
	int num=0, n, i, arr[10];
	
	printf("Enter a value for n: ");
	scanf("%d", &n);
	for(i=0;i<n;i++)
	{
		printf("Enter arr[%d]=", i);
		scanf("%d", &arr[i]);
		num=num+arr[i] * pow(10,i);
	}
	
	printf("The num is %d\n", num);
	
	return 0;
	
}
