// page41_22.c
// add prime nums from m to n
#include<stdio.h>

int main()
{
	int m, n, i, flag, sum=0;
	scanf("%d%d", &m,&n);
	while (m < n)
    {
        flag = 0;
        for(i = 2; i <= m/2; ++i)
        {
            if(m % i == 0)
            {
                flag = 1;
                break;
            }
        }
        if (flag == 0)
            sum+=m;
        ++m;
    }
    
    printf("The sum is %d\n", sum);
    
    return 0;
}
