// Program to read 2D array marks which stores the marks of five students in three subjects. Write a program to display the highest marks in each subject.c
#include<stdio.h>

int main()
{
	int marks[5][3], i, j, max;
	
	for(i=0; i<5;i++)
	{
		printf("Enter the marks obtained by student %d\n", i+1);
		for(j=0;j<3;j++)
		{
			printf("marks[%d][%d]=", i, j);
			scanf("%d", &marks[i][j]);
		}
	}
	
	for(j=0;j<3;j++)
	{
		max = -999;
		for(i=0; i<5; i++)
		{
			if(marks[i][j]>max)
				max = marks[i][j];
		}
		printf("The highest marks botained in the subject %d = %d\n", j+1, max);
	}
	
	return 0;
	
}

