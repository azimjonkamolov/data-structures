// passing array's element's address to anouther function.c
#include<stdio.h>

void func(int *num)
{
	printf("%d", *num);
}

int main()
{
	int a[5]={1,2,3,4,5};
	func(&a[3]);
}
