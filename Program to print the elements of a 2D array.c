// Program to print the elements of a 2D array.c
#include<stdio.h>

int main()
{
	int arr[2][2]={11, 22, 33, 44};
	int i, j;
	
	for(i=0;i<2;i++)
	{
		for(j=0;j<2;j++)
			printf("%d\t", arr[i][j]);
			
		printf("\n");		
	}

	return 0;
}
