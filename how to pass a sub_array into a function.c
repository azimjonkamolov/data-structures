// how to pass a sub_array into a function.c
#include<stdio.h>

// called function
void func(int *a1, int length)
{
	int i;
	for(i=0;i<length;i++)
		printf("%d ", a1[i]);
}

// calling function
int main()
{
	int a[10]={1,2,3,4,5,6,7,8,9,10};
	func(&a[2],8);
	
}
