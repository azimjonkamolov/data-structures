// Program to find the mean of n numbers using arrays.c
#include<stdio.h>

int main()
{
	int arr[20], i, n, sum=0;
	float mean;
	
	printf("Enter the value for n: ");
	scanf("%d", &n);
	
	for(i=0;i<n;i++)
	{
		printf("Enter the value for arr[%d]= ", i);
		scanf("%d", &arr[i]);
		sum+=arr[i];
	}
	mean = (float)sum/n;
	
	printf("The mean of this array is %.2f\n", mean);
	
	return 0;
	
	
}
