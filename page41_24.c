// page41_24.c
// program to add two floating point numbers using pointers and functions
#include<stdio.h>

float sum(float*, float*);

int main()
{
	float a, b;
	scanf("%f%f", &a, &b);
	sum(&a,&b);
	
	return 0;
}

float sum(float* a, float* b)
{
	printf("The sum is %.2f\n", *a+*b);
}










//
