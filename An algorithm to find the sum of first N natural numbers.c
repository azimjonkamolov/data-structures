// An algorithm to find the sum of first N natural numbers.c
#include<stdio.h>

int main()
{
	int n, i, sum;
	// Step 1: Input N
	scanf("%d", &n);
	// Step 2: SET I = 1, SUM = 0
	i = 1, sum = 0;
	// Step 3: Repeat Step 4 while I<=N
	while (i<=n)
	{
		// Step 4: SET SUM = SUM + I >> SET I = I + 1
		sum = sum + i;
		i = i + 1;		
	}
	
	// Step 5: PRINT SUM
	printf("The sum is %d\n", sum);
	
	// END
	return 0;

}
