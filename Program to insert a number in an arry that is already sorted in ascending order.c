// Program to insert a number in an arry that is already sorted in ascending order.c

#include<stdio.h>

int main()
{
	int i, n, pos, j, arr[10], num;
	
	printf("Enter the size of an array: ");
	scanf("%d", &n);
	
	for(i=0;i<n;i++)
	{
		printf("arr[%d]=", i);
		scanf("%d", &arr[i]);
	}
	
	printf("Enter a value for num: ");
	scanf("%d", &num);
	

	for(i=0;i<n;i++)
	{
		if(arr[i]>num)
		{
			for(j=n-1;j>=i;j--)
			{
				arr[j+1]=arr[j];
			}
			arr[i]=num;
			break;
		}
		else if(arr[i]<num)
		{
			arr[n]=num;
		}
	}
	n=n+1;
	
	printf("Array after insertion of %d is : ", num);
	for(i=0;i<n;i++)
	{
		printf("arr[%d]=", i);
		printf("%d\n", arr[i]);
	}
	
	return 0;
	
}
