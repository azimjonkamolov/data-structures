// Arrays to find the wether the array of integers contains a duplicate number.c
// Author: Azimjon Kamolov
// Operations on Arrays
// * Traversing an array <<<
// * Inserting an element in an array
// * Searching an element in an array
// * Deleting an element form an array
// * Merging two arrays
// * Sorting an array in ascending or descending order

#include<stdio.h>

int main()
{
	int arr[10], i, n, j, flag=0;
	printf("Enter the size of the array: ");
	scanf("%d", &n);
	
	for(i=0;i<n;i++)
	{
		scanf("%d", &arr[i]);
	}
	
	for(i=0;i<n;i++)
	{
		for(j=i+1; j<n; j++)
		{
			if(arr[i]==arr[j] && i!=j)
			{
				flag=1;
				printf("Dublicate nums are found at location of %d and %d\n", i,j);
			}
		}
	}
	
	if(flag==0)
		printf("Dublicate nums are not found");
	
	
	return 0;
}
