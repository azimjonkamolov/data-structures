// page41_17.c
// program to read a character and print it.
// Also print its ASCII value. If the character is in
// lower case, print it in upper case and vice versa.
// Repeat the process until a �*� is entered.

#include<stdio.h>

int main()
{
	char c;
	
	printf("Enter a character please: ");
	
	while(1)
	{
	scanf("%1c", &c);
	if(c=='*')
		break;
	if(c!='\n')
	{
	printf("The char entered is %c\n", c);
	printf("The char's ASCII value is %d\n", c);
	printf("The char's other version is ");
	if(c >= 65 && c <=90)
		printf("%c\n", c+32);
	else if(c >= 97 && c <=122)
		printf("%c\n", c-32);
	else
		printf("itself\n");
	
	printf("\n");
	printf("Enter a character please: ");	
	}
	}
	
	return 0;

}













//
