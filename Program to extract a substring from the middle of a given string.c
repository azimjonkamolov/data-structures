// Program to extract a substring from the middle of a given string.c
#include<stdio.h>
#include<string.h>

int main()
{
	char str[20], substr[20];
	int i=0, j=0, n, m;
	printf("Enter the main string: ");
	gets(str);
	printf("Enter a position from which to start the substring: ");
	scanf("%d", &m);
	printf("Enter the length of the substring: ");
	scanf("%d", &n);
	i=m;
	while(str[i]!='\0' && n>0)
	{
		substr[j]=str[i];
		i++;
		j++;
		n--;
	}
	substr[j] = '\0';
	printf("The substring is: ");
	puts(substr);
	
	return 0;
}
