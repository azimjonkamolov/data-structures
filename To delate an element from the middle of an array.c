// To delate an element from the middle of an array
#include<stdio.h>

int main()
{
	int a[10], i, n=7, pos=3, dnum; // do delate an item in the position of 7th
	
	printf("Enter 7 elements into the array:\n");
	for(i=0;i<n;i++)
		scanf("%d", &a[i]);
	
	i=pos;
	while(i<=n-1)
	{
		a[i]=a[i+1];
		i=i+1;
	}
	n=n-1;
	
	printf("Array after: ");
	for(i=0;i<n;i++)
	{
		printf("%d ", a[i]);
	}
}
