// Program to insert a value into a given position in an array.c
#include<stdio.h>

int main()
{
	int n, i, arr[10], pos, num;
	printf("Enter n value for an array: ");
	scanf("%d", &n);
	for(i=0;i<n;i++)
	{
		printf("Enter a value for arr[%d]=", i);
		scanf("%d", &arr[i]);
	}
	printf("Enter a value to be inserted: ");
	scanf("%d", &num);
	printf("Enter a value for the position: ");
	scanf("%d", &pos);
	for(i=n-1;i>=pos;i--)
	{
		arr[i+1]=arr[i];
	}
	arr[pos]=num;
	n=n+1;
	for(i=0;i<n;i++)
	{
		printf("arr[%d]=", i);
		printf("%d\n", arr[i]);
	}
	
	return 0;
}
