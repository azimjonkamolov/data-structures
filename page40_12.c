// page40_12.c
// program that prints a floating point
// value in exponential format with the following specifications
#include<stdio.h>

int main()
{
	float num;
	printf("Enter a float number please: ");
	scanf("%f", &num);
	printf("%.2f\n", num);
	printf("%.4f\n", num);
	printf("%.8f\n", num);
	
	return 0;
	
}
