// Program to multiply two m x n matrx.c
#include<stdio.h>
#include<conio.h>

int main()
{
	int i, j;
	int rows1, cols1, rows2, cols2, rows_sum, cols_sum;
	int mat1[5][5], mat2[5][5], sum[5][5];
	
	printf("Enter the number of rows in the first matrix: ");
	scanf("%d", &rows1);
	printf("Enter the number of columns in the first matrix: ");
	scanf("%d", &cols1);
	printf("Enter the number of rows in the second matrix: ");
	scanf("%d", &rows2);
	printf("Enter the number of columns in the second matrix: ");
	scanf("%d", &cols2);
	
	if(rows1 != rows2 || cols1 != cols2)
	{
		printf("Number of rows and columns of both matrices must be equal:\n");
		return 0;
	}
	rows_sum = rows1;
	cols_sum = cols1;
	
	printf("Enter the elements of the first matrix:\n");
	for(i=0;i<rows1;i++)
	{
		for(j=0; j<cols1;j++)
		{
			scanf("%d", &mat1[i][j]);
		}
	}
	
	printf("Enter the elements of the second matrix:\n");
	for(i=0;i<rows2;i++)
	{
		for(j=0; j<cols2;j++)
		{
			scanf("%d", &mat2[i][j]);
		}
	}
	
	for(i=0;i<rows_sum;i++)
	{
		for(j=0;j<cols_sum;j++)
		{
			sum[i][j]=mat1[i][j] * mat2[i][j];	
		}	
	}

	printf("The elements of the product matrix are:\n");
	for(i=0;i<rows_sum;i++)
	{

		for(j=0;j<cols_sum;j++)
		{
			printf("%d\t", sum[i][j]);
		}
		
		printf("\n");
	}
	
	return 0;
}
