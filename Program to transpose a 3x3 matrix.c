// Program to transpose a 3x3 matrix.c
#include<stdio.h>

int main()
{
	int i, j, mat[3][3], trans_mat[3][3];
	
	printf("Enter the elements of the matrix:\n");
	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			scanf("%d", &mat[i][j]);
		}
	}
	
	printf("The elements of the array are:\n");
	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
		{
			printf("%d ", mat[i][j]);
		}
		printf("\n");
	}
	
	printf("The trans matrix elements are:\n");
	for(i=0;i<3;i++)
	{

		for(j=0;j<3;j++)
		{
			printf("%d ", mat[j][i]);
		}
		printf("\n");
	}
	
	return 0;
}
