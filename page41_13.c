// page41_13.c
// Write a program to find the smallest of three integers using functions

#include<stdio.h>

float min(float *, float *, float *);

int main()
{
	float n, n1, n2, res;
	printf("Enter 3 nums: ");
	scanf("%f %f %f", &n, &n1, &n2);
	res = min(&n,&n1,&n2);	// TO SEND THE ADDRESS
	printf("The smallest number among them is %.2f\n", res);
	
	return 0;
}

float min(float* a, float* b, float* c)
{
	if(*a<*b && *a<*c)
		return *a;
	if(*b<*a && *b<*c)
		return *b;
	if(*c<*b && *c<*a)
		return *c;	
}










//
