// Program to enter n number of digits.Form a number ousing these digits.c
#include<stdio.h>
#include<math.h>

int main()
{
	int arr[10], n, i, num=0;
	printf("Enter a value for n: ");
	scanf("%d", &n);
	for(i=0;i<n;i++)
	{
		printf("Enter a value for arr[%d]=", i);
		scanf("%d", &arr[i]);
		num += arr[i] * pow(10,i);
	}
	
	printf("The number is : %d\n", num);
	
	return 0;
}
