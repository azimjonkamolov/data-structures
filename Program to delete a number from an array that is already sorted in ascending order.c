// Program to delete a number from an array that is already sorted in ascending order.c
#include<stdio.h>

int main()
{
	int i, n, pos, arr[10], num, j, flag=0;
	
	printf("Enter a number for the size of array: ");
	scanf("%d", &n);
	
	for(i=0;i<n;i++)
		arr[i]=i;
		
	printf("Enter a number to be deleted: ");
	scanf("%d", &num);
	
	for(i=0;i<n;i++)
	{
		if(arr[i]==num)
		{
			for(j=i;j<n-1;j++)
			{
				arr[j]=arr[j+1];
			}
			flag=1;
		}
	}
	
	n=n-1;
	if(flag==0)
	{
		printf("The num is not found!\n");
	}
	else
	{
		for(i=0;i<n;i++)
		{
			printf("arr[%d]=", i);
			printf("%d\n", arr[i]);
		}		
	}

	
	return 0;
}
