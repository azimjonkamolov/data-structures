// Program to merge two sorted arrays into one.c
#include<stdio.h>

int main()
{
	int arr1[10], arr2[10], arr3[20];
	int in1, in2, in3, n1, n2, n3;
	
	printf("Enter a value for n1: ");
	scanf("%d", &n1);
	
	for(in1=0;in1<n1;in1++)
		scanf("%d", &arr1[in1]);
		
	printf("Enter a value for n2: ");
	scanf("%d", &n2);
	
	for(in2=0;in2<n2;in2++)
		scanf("%d", &arr2[in2]);
		
	n3=n1+n2;
	in1=0;
	in2=0;
	
	while(in1<n1 && in2<n2)
	{
		printf("inside while\n");
		if(arr1[in1]<=arr2[in2])
		{
			arr3[in3]=arr1[in1];
			printf("arr3[%d]=%d\n", in3, arr3[in3]);
			in1++;
		}
		else
		{
			arr3[in3]=arr2[in2];
			printf("arr3[%d]=%d\n", in3, arr3[in3]);
			in2++;				
		}
		in3++;
	}
	
	printf("in1 = %d\n", in1);
	printf("in2 = %d\n", in2);
	printf("in3 = %d\n", in3);
		
	if(in1==n1)
	{
		while(in2<n2)
		{
			arr3[in3]=arr2[in2];
			printf("arr3[%d]=%d\n", in3, arr3[in3]);
			in2++;
			in3++;			
		}	
	}
	else if(in2==n2)
	{
		while(in1<n1)
		{
			arr3[in3]=arr1[in1];
			printf("arr3[%d]=%d\n", in3, arr3[in3]);
			in1++;
			in3++;			
		}
	}		
	
	for(in3=0;in3<n3;in3++)
		printf("%d ", arr3[in3]);
	
	return 0;		
			
}
