// Arrays to enter n number of digits and form a number using these digits.c
// Author: Azimjon Kamolov
// Operations on Arrays
// * Traversing an array <<<
// * Inserting an element in an array
// * Searching an element in an array
// * Deleting an element form an array
// * Merging two arrays
// * Sorting an array in ascending or descending order
#include<stdio.h>

int main()
{
	int num=0, arr[10], n, i;
	
	printf("Enter the number of digits: ");
	scanf("%d", &n);
	for(i=0;i<n;i++)
	{
		scanf("%d", &arr[i]);
	}
	i=0;
	while(i<n)
	{
		num = num + arr[i] * pow(10,i);
		i++;
	}
	printf("The num: %d\n", num);
	
	return 0;
}
