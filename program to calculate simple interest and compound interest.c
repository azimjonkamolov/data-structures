// program to calculate simple interest and compound interest.c
#include<stdio.h>
#include<math.h>

// float data type has been used for this program

float sim_int(float,float,float);	// FUNCTION TO FIND A SIMPLE INTEREST
float com_int(float,float,float);	// FUNCTION TO FIND A COMPOUND INTEREST

int main()
{
	float res, p, r, t;
	printf("Enter Principle, rate and time: ");
    scanf("%f%f%f", &p,&r,&t);
    res = sim_int(p,r,t);
    printf("Simple interest of %.1f, %.1f and %.1f is %.1f\n",p,r,t, res);
    res = com_int(p,r,t);
    printf("Compound interest of %.1f, %.1f and %.1f is %.1f\n",p,r,t, res);
    
    return 0;
	
}

float sim_int(float p,float r,float t)
{
    return (p * r * t)/100;
}

float com_int(float p, float r, float t)
{
	return p*pow((1+r/100),t);
}











//
