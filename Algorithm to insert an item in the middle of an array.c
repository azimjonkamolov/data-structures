// Algorithm to insert an item in the middle of an array.c
#include<stdio.h>

int main()
{
	int a[10], i, n=10, pos=5;
	
	
	for(i=0;i<n;i++) // To insert an ellement in an array
	{
		printf("Enter an element in the array: ");
		scanf("%d", &a[i]);
	}
	
	i = n; // n is a final number here
	
	while(i>=pos) // if i is higher than pos it keeps running
	{
		a[i+1]=a[i]; // all the elements in the array is moving one time forward
		i=i-1; // every times it reduces 
	}
	
	n=n+1; // so the size of array increases too :)
	a[pos]= 17; // 17 is inserted into the position
	
	printf("Elements in the array: ");
	
	for(i=0;i<n;i++) // n has been increased now it is 11 now :)
	{
		printf("%d ", a[i]);	
	}
	
	return 0;
}
