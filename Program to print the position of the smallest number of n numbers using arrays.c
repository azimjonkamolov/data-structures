// Program to print the position of the smallest number of n numbers using arrays.c
#include<stdio.h>

int main()
{
	int i, n, arr[20], small, pos;
	
	printf("Enter the value for n: ");
	scanf("%d", &n);
	
	for(i=0;i<n;i++)
	{
		printf("Enter the value for arr[%d]=", i);
		scanf("%d", &arr[i]);
	}
	
	small=arr[0];
	pos=0;
	
	for(i=0;i<n;i++)
	{
		if(arr[i]<small)
		{
			small=arr[i];
			pos=i;
		}
	}
	
	printf("The smallest element is %d\n", small);
	printf("The position of the smallest element is %d\n", pos);
	
	return 0;
	
	
}
