// passing the entire array.c
#include<stdio.h>

void func(int arr[7])
{
	int i;
	for(i=0;i<5;i++)
		printf("%d ", arr[i]);
}

int main()
{
	int a[5]={1,2,3,4,5};
	func(a); // just simply by name
}
