// Arrays to find the mean of n numbers using arrays.c
// Author: Azimjon Kamolov
// Operations on Arrays
// * Traversing an array <<<
// * Inserting an element in an array
// * Searching an element in an array
// * Deleting an element form an array
// * Merging two arrays
// * Sorting an array in ascending or descending order
#include<stdio.h>

int main()
{
	int i, n, arr[15], sum = 0;
	float mean = 0.0;
	printf("Enter the number of elements in the array: ");
	scanf("%d", &n);
	for(i=0;i<n;i++)
	{
		printf("arr[%d]=", i);
		scanf("%d", &arr[i]);
		sum+=arr[i];
	}
	
	mean = (float)sum/n;
	printf("The sum of the arrs: %d\n", sum);
	printf("The mean of the arrs: %.2f", mean);
	
	
	return 0;
}
