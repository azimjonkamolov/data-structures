// Program to intercharge the largest and the smallest number in an array.c
#include<stdio.h>

void readArray(int arr[], int n, int i);
void displayArray(int arr[], int n, int i);
void intArray(int arr[], int n, int i);
int bigNumber(int arr[], int n, int i);
int smallNumber(int arr[], int n, int i);


int main()
{
	int arr[10], i, n;
	
	printf("Enter a value for n: ");
	scanf("%d", &n);
	readArray(arr,n,i);
	smallNumber(arr,n,i);
	bigNumber(arr,n,i);
	intArray(arr,n,i);
	displayArray(arr,n,i);
	
	return 0;
}

void readArray(int arr[10], int n, int i)
{
	for(i=0;i<n;i++)
	{
		printf("arr[%d]=", i);
		scanf("%d", &arr[i]);
	}
}

void displayArray(int arr[10], int n, int i)
{
	printf("After all:\n");
	for(i=0;i<n;i++)
	{
		printf("%d\n", arr[i]);
	}
}

void intArray(int arr[10], int n, int i)
{
	int big, small, temp;
	big = bigNumber(arr,n,i);
	small = smallNumber(arr,n,i);
	printf("Before the swap %d %d\n", big, small);
	temp = arr[big];
	arr[big] = arr[small];
	arr[small] = temp;
	printf("After the swap %d %d\n", big, small);
//	
//	for(i=0;i<n;i++)
//	{
//		printf("arr[%d]=%d\n", i, arr[i]);
//	}
}

int bigNumber(int arr[10], int n, int i)
{
	int big = arr[0], pos=0;
	for(i=0;i<n;i++)
	{
		if(big<arr[i])
		{
			big=arr[i];
			pos = i;
		}
	}
	
	return pos;
}


int smallNumber(int arr[10], int n, int i)
{
	int small = arr[0], pos=0;
	for(i=0;i<n;i++)
	{
		if(small>arr[i])
		{
			small=arr[i];
			pos=i;
		}
	}
	return pos;
}







