// Program to reverse a string using strrev builtin func.c
#include<stdio.h>
#include<string.h>

int main()
{
	char str[20];
	
	printf("Enter a string here: ");
	scanf("%s", str);
	printf("The reversed string: %s\n", strrev(str));
	
	return 0;
}
