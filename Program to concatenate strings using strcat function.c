// Program to concatenate strings using strcat function.c

#include<stdio.h>
#include<string.h>

int main()
{
	char firstName[20], lastName[20];
	
	printf("Enter your fistr name please: ");
	scanf("%s", firstName);
	printf("Enter your last name please: ");
	scanf("%s", lastName);
	
	strcat(firstName, " ");
	strcat(firstName, lastName);
	
	printf("Your full name is %s\n", firstName);
	
	return 0;
}
