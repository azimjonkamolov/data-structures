// Program to read and display a 2x2 array.c
#include<stdio.h>

int main()
{
	int arr[2][2][2], i, j, k;
	
	printf("Enter the elements of the matrix: ");
	
	for(i=0;i<2;i++)
	{
		for(j=0;j<2;j++)
		{
			for(k=0;k<2;k++)
			{
				scanf("%d", &arr[i][j][k]);
			}
		}
	}
	
	printf("\nThe matrix is :\n");
	for(i=0;i<2;i++)
	{
		for(j=0;j<2;j++)
		{
			for(k=0;k<2;k++)
			{
				printf("arr[%d][%d][%d] = %d\n", i, j, k, arr[i][j][k]);
			}
		}
	}
	
	return 0;	
}
