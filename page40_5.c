// page40_5.c
// Write a program to read two floating point
// numbers. Add these numbers and assign the result
// to an integer. Finally, display the value of all the
// three variables.
#include<stdio.h>

int main()
{
	float num1, num2;
	int sum;
	printf("Enter two float values: ");
	scanf("%f %f", &num1, &num2);
	sum = num1 + num2;
	printf("The result of %.2f plus %.2f is %d\n", num1, num2, sum);
	
	return 0;
}










//
