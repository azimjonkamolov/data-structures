// Program to delete a number from a given location in an arry.c
#include<stdio.h>

int main()
{
	int arr[10], n, i, pos;
	printf("Enter a value for n: ");
	scanf("%d", &n);
	for(i=0;i<n;i++)
		arr[i]=i;
	printf("Enter a value for pos: ");
	scanf("%d", &pos);
	for(i=pos; i<n-1;i++)
	{
		arr[i]=arr[i+1];
	}
	n-=1;
	
	for(i=0;i<n;i++)
	{
		printf("arr[%d]=",i);
		printf("%d\n", arr[i]);
	}
	
	return 0;
}
