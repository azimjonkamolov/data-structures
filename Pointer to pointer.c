// Pointer to pointer.c

#include<stdio.h>

int main()
{
	int a = 10;
	int *pa, **ppa, ***pppa;
	
	pa = &a;
	ppa = &pa;
	pppa = &ppa;
	
	printf("a: %d\n", a);	// THIS IS A VALUE
	printf("*pa: %d\n", *pa);	// THIS IS AN ADDRESS
	printf("**ppa: %d\n", **ppa);	// THIS IS AN ADDRESS
	printf("***pppa: %d\n", ***pppa);	// THIS IS AN ADDRESS	
	
	// THIS WORKS :)
	
	return 0;
}
