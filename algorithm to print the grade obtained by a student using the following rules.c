// An algorithm to print the grade obtained by a student using the following rules.c
#include<stdio.h>

int main()
{
	int m;
	// Step 1: Enter the Marks obtained as M
	scanf("%d", &m);
	
	// Step 2: IF M > 75 THEN GIVE O
	if (m>75)
		printf("O\n");
	// Step 3: IF M > 60 AND M < 75 THEN GIVE A
	if (m>=60 && m<=75)
		printf("A\n");
	// Step 4: IF M > 50 AND M < 60 THEN GIVE B
	if (m>=50 && m<=60)
		printf("B\n");
	// Step 5: IF M > 40 AND M < 50 THEN GIVE C
	if (m>=40 && m<=50)
		printf("C\n");
	// Step 6: ELSE IF M < 40 THEN GIVE D
	else if(m<40)
		printf("D");
		
	// END
	return 0;
	
	
}







//
