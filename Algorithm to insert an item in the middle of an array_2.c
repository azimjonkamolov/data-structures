// Algorithm to insert an item in the middle of an array_2.c
#include<stdio.h>

int main()
{
	int a[7], i, n=5, pos=3, num; // once declared you cant increase size of array on the fly
	
	for(i=0;i<n;i++)
	{
		printf("Enter an element in an array: ");
		scanf("%d", &a[i]);
	} 
	
	i=n;
	
	while(i>=pos)
	{
		a[i+1]=a[i];
		i-=1;
	}
	n=n+1;
	
	printf("\nEnter the desired number: ");
	scanf("%d", &num);
	
	a[pos]=num;
	
	printf("The elements in the array are ");
	
	for(i=0;i<n;i++)
	{
		printf("%d ", a[i]);
	}
	
	return 0;
}
