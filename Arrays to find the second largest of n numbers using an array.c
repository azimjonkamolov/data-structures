// Arrays to find the second largest of n numbers using an array.c
// Author: Azimjon Kamolov
// Operations on Arrays
// * Traversing an array <<<
// * Inserting an element in an array
// * Searching an element in an array
// * Deleting an element form an array
// * Merging two arrays
// * Sorting an array in ascending or descending order
#include<stdio.h>

int main()
{
	int i, n, arr[20], large, second_large;
	printf("Enter teh number of elements in the array: ");
	scanf("%d", &n);
	printf("Enter the elements: ");
	for(i=0;i<n;i++)
	{
		scanf("%d", &arr[i]);
	}
	large = arr[0];
	for(i=0;i<n;i++)
	{
		if(large < arr[i])
		{
			second_large=large;
			large=arr[i];
		}
	}
	printf("The second largest number: %d\n", second_large);

	return 0;
}
